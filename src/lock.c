#include<stdlib.h>
#include<stdio.h>
#include<poll.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>

#define RED 	"24"
#define GREEN 	"23"
#define WHITE	"18"
#define BLUE	"17"

#define LEFT	"10"
#define RIGHT	"22"
#define TOP	"27"

#define ON	1
#define OFF	0

#define MAX_LEN	5

#define GPIO_DIR "/sys/class/gpio/gpio"

#define BLINK_TIME 300 * 1000

#define FAILS_TO_ALARM 3

int code_len = -1;
int code[MAX_LEN];

int prepare_led(int export_fd, const void* buf, size_t nbyte)
{
	int direction;

	char direction_path[80];
	strcpy(direction_path, GPIO_DIR);
	strcat(direction_path, buf);
	strcat(direction_path, "/direction");
	
	write(export_fd, buf, nbyte);
	direction = open(direction_path, O_WRONLY, 0666);
	write(direction, "out", sizeof("out"));
	close(direction);
	
	//TODO: errorcheck	
	return EXIT_SUCCESS;
}

int set_led(const void* buf, int value)
{
	int led;

	char led_path[80];
	strcpy(led_path, "/sys/class/gpio/gpio");
	strcat(led_path, buf);
	strcat(led_path, "/value");
	
	led = open(led_path, O_WRONLY, 0666);

	//da się ładniej
	if (value)
		write(led, "1", sizeof("1"));
	else
		write(led, "0", sizeof("1"));

	close(led);
	
	//TODO: errorcheck	
	return EXIT_SUCCESS;
}

int prepare_button(int export_fd, const void* buf, size_t nbyte)
{
	int direction, edge;

	char direction_path[80];
	strcpy(direction_path, GPIO_DIR);
	strcat(direction_path, buf);
	strcat(direction_path, "/direction");

	char edge_path[80];
	strcpy(edge_path, GPIO_DIR);
	strcat(edge_path, buf);
	strcat(edge_path, "/edge");
	
	write(export_fd, buf, nbyte);
	
	direction = open(direction_path, O_WRONLY, 0666);
	write(direction, "in", sizeof("in"));
	close(direction);

	edge = open(edge_path, O_WRONLY, 0666);
	write(edge, "falling", sizeof("falling"));
	close(edge);
	
	//TODO: errorcheck	
	return EXIT_SUCCESS;
}

int button_open_read(const void* buf, size_t nbyte, struct pollfd* pfd)
{
	int fd;

	char value_path[80];
	strcpy(value_path, GPIO_DIR);
	strcat(value_path, buf);
	strcat(value_path, "/value");

	fd = open(value_path, O_RDONLY);
	pfd->fd = fd;
	pfd->events = POLLPRI;

	return fd;
}

void print_code()
{
	int i;
	for (i = 0; i <= code_len; ++i)
		printf("%d", code[i]);
	printf("\n");
}


int create_code()
{
	struct pollfd fdlist[3];

	fdlist[0].fd = button_open_read(LEFT, sizeof(LEFT), &fdlist[0]);
	fdlist[1].fd = button_open_read(RIGHT, sizeof(RIGHT), &fdlist[1]);
	fdlist[2].fd = button_open_read(TOP, sizeof(TOP), &fdlist[2]);

	char buf[3];

	int n = 0;
	int code_provided = 0;

	while(n < MAX_LEN && !code_provided)
	{
		int err;
		err = poll(fdlist, 3, -1);

		if (err > 0)
		{
			int i;
			for (i = 0; i < 3; ++i)
			{
				if (fdlist[i].revents & POLLPRI)
				{
					read(fdlist[i].fd, buf, sizeof(buf));
					lseek(fdlist[i].fd, 0, SEEK_SET);

					if (buf[0] == '0')
					{
						switch(i)
						{
							case 0:
								printf("%d. White - 0\n", n);
								code[n] = 0;
								set_led(WHITE, ON);
								usleep(BLINK_TIME);
								set_led(WHITE, OFF);
								n++;
								break;
							case 1:
								printf("%d. Blue - 1\n", n);
								code[n] = 1;
								set_led(BLUE, ON);
								usleep(BLINK_TIME);
								set_led(BLUE, OFF);
								n++;
								break;
							case 2:
								if (n > 0)
									code_provided = 1;
								break;
						}
					}
				}
			}
		}
	}

	code_len = n - 1;
	set_led(RED, OFF);
	set_led(GREEN, OFF);

	print_code();

	close(fdlist[0].fd);
	close(fdlist[1].fd);
	close(fdlist[2].fd);
	

	return EXIT_SUCCESS;
}

int guess_code()
{
	struct pollfd fdlist[3];

	fdlist[0].fd = button_open_read(LEFT, sizeof(LEFT), &fdlist[0]);
	fdlist[1].fd = button_open_read(RIGHT, sizeof(RIGHT), &fdlist[1]);
	fdlist[2].fd = button_open_read(TOP, sizeof(TOP), &fdlist[2]);

	char buf[3];

	int n, i, fail;
	int tries = 0;

	n = 0; fail = 0;
	while(n <= code_len && fail < FAILS_TO_ALARM)
	{
		int err;
		err = poll(fdlist, 3, -1);

		if (err > 0)
		{
			int i;
			for (i = 0; i < 2; ++i)
			{
				if (fdlist[i].revents & POLLPRI)
				{
					read(fdlist[i].fd, buf, sizeof(buf));
					lseek(fdlist[i].fd, 0, SEEK_SET);

					if (buf[0] == '0')
					{
						printf("%d| %d : %d\n", n, i, code[n]);

						if (i == code[n])
						{
							set_led(GREEN, ON);
							usleep(BLINK_TIME);
							set_led(GREEN, OFF);
							usleep(BLINK_TIME);
							n++;
						}
						else
						{
							fail = fail + 1;
							for (i = 0; i < 3; ++i)
							{
								set_led(RED, ON);
								usleep(BLINK_TIME);
								set_led(RED, OFF);
								usleep(BLINK_TIME);
							}
							n = 0;
						}
					}
				}
			}
		}	
	}

	if (fail == FAILS_TO_ALARM)
	{
		for (i = 0; i < 10; ++i)
		{
			set_led(GREEN, ON);
			set_led(RED, ON);
			set_led(BLUE, ON);
			set_led(WHITE, ON);
			usleep(BLINK_TIME);
			set_led(GREEN, OFF);
			set_led(RED, OFF);
			set_led(BLUE, OFF);
			set_led(WHITE, OFF);
			usleep(BLINK_TIME);
		}
	}
	else
	{
		for (i = 0; i < 3; ++i)
		{
			set_led(GREEN, ON);
			usleep(BLINK_TIME);
			set_led(GREEN, OFF);
			usleep(BLINK_TIME);
		}
	}

	close(fdlist[0].fd);
	close(fdlist[1].fd);
	close(fdlist[2].fd);
	

	return EXIT_SUCCESS;
}

int initialize_gpio()
{
	int export_fd = open("/sys/class/gpio/export", O_WRONLY, 0666);
	prepare_led(export_fd, BLUE, sizeof(BLUE));
	prepare_led(export_fd, WHITE, sizeof(WHITE));
	prepare_led(export_fd, GREEN, sizeof(GREEN));
	prepare_led(export_fd, RED, sizeof(RED));

	prepare_button(export_fd, LEFT, sizeof(LEFT));
	prepare_button(export_fd, RIGHT, sizeof(RIGHT));
	prepare_button(export_fd, TOP, sizeof(TOP));
	close(export_fd);

	return EXIT_SUCCESS;
}

int main()
{
	initialize_gpio();

	set_led(BLUE, OFF);
	set_led(GREEN, ON);
	set_led(RED, ON);
	set_led(WHITE, OFF);
	
	create_code();
	guess_code();

	return EXIT_SUCCESS;
}
